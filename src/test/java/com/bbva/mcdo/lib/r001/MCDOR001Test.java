package com.bbva.mcdo.lib.r001;
/*
import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;
import javax.annotation.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/META-INF/spring/MCDOR001-app.xml",
		"classpath:/META-INF/spring/MCDOR001-app-test.xml",
		"classpath:/META-INF/spring/MCDOR001-arc.xml",
		"classpath:/META-INF/spring/MCDOR001-arc-test.xml" })
public class MCDOR001Test {

	private static final Logger LOGGER = LoggerFactory.getLogger(MCDOR001Test.class);

	@Resource(name = "mcdoR001")
	private MCDOR001 mcdoR001;

	@Resource(name = "applicationConfigurationService")
	private ApplicationConfigurationService applicationConfigurationService;

	@Before
	public void setUp() throws Exception {
		ThreadContext.set(new Context());
		getObjectIntrospection();
	}
	
	private Object getObjectIntrospection() throws Exception{
		Object result = this.mcdoR001;
		if(this.mcdoR001 instanceof Advised){
			Advised advised = (Advised) this.mcdoR001;
			result = advised.getTargetSource().getTarget();
		}
		return result;
	}
	
	@Test
	public void executeTest(){
		LOGGER.info("Executing the test...");
		mcdoR001.execute();
	}
	
}
*/
