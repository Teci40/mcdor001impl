package com.bbva.mcdo.lib.r001.impl.repository;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.codehaus.jackson.JsonNode;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.SerializationFeature;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
//import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResponseExtractor;

import com.bbva.elara.utility.api.connector.APIConnector;
import com.bbva.mcdo.lib.r001.impl.utils.Impl001FieldsEnum;
import com.bbva.mcdo.lib.r001.impl.utils.Impl001LogErrorEnum;
import com.bbva.mcdo.lib.r001.impl.utils.Impl001LogInfoEnum;
import com.bbva.mcdo.lib.r001.impl.utils.Impl001ResponseCodeEnum;
import com.bbva.mcdo.lib.r001.impl.utils.Impl001Utils;
import com.bbva.mcdo.lib.r001.impl.utils.Impl001ValuesEnum;
import com.bbva.mcdo.dto.salesforce.closeJob.CloseJobInDTO;
import com.bbva.mcdo.dto.salesforce.connection.ConnectionInDTO;
import com.bbva.mcdo.dto.salesforce.connection.ConnectionOutDTO;
import com.bbva.mcdo.dto.salesforce.createJob.CreateJobInDTO;
import com.bbva.mcdo.dto.salesforce.sendFile.SendFileInDTO;
import com.bbva.mcdo.dto.salesforce.showResults.ShowResultsOutDTO;


public class Impl001Repository {

	private static Logger LOGGER = LoggerFactory.getLogger(Impl001Repository.class);
	private static final String ERROR_HTTP = "Failed : HTTP error code : {}";
	public final static MediaType TEXT_CSV_TYPE = new MediaType("text", "csv");
	
	
	/**
	 * Create connection with external api connector
	 * Crear la conexion con el conector
	 * @param apiConnectorExt
	 * @param connectionIn
	 * @param connectionOut
	 * @return HttpStatus
	 * @throws IOException
	 */
	public HttpStatus createConnectionExternalAPI(final APIConnector apiConnectorExt,final ConnectionInDTO connectionIn, 
			final ConnectionOutDTO connectionOut)throws IOException {
		LOGGER.info(Impl001LogInfoEnum.REPO_INI_METHOD.getPattern(),"createConnectionExternalAPI");

		// Se establecen los parametros de conexion
		final LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.set(Impl001FieldsEnum.CLIENT_ID.getField(), connectionIn.getConsumerKey());
		params.set(Impl001FieldsEnum.CLIENT_SECRET.getField(), connectionIn.getConsumerSecret());
		params.set(Impl001FieldsEnum.GRAND_TYPE.getField(), Impl001FieldsEnum.PASSWORD.getField());
		params.set(Impl001FieldsEnum.USERNAME.getField(), connectionIn.getUsername());
		params.set(Impl001FieldsEnum.PASSWORD.getField(), connectionIn.getPassword());
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"Params",params);

		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"HttpHeaders",headers);

		// Se establece un HttpEntity para añadir las cabeceras
		final HttpEntity<LinkedMultiValueMap<String, String>> entity = new HttpEntity<LinkedMultiValueMap<String, String>>(
				params, headers);
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"HttpEntity",entity);

		// Se hace la conexion
		final ResponseEntity<String> jsonPost;
		try {
			jsonPost = apiConnectorExt.exchange(Impl001ValuesEnum.GET_URL_SALESFORCE.getValue(), HttpMethod.POST,
					entity, String.class);
			LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"ResponseEntity",jsonPost);
		} catch (HttpClientErrorException e) {
			LOGGER.info(Impl001LogErrorEnum.EXCEPTION_MESSAGE_PATTERN.getPattern(),"HttpClientErrorException" ,e.getMessage());
			LOGGER.info(Impl001LogErrorEnum.EXCEPTION_TOSTRING_PATTERN.getPattern(),"HttpClientErrorException", e.toString());
			throw e;
		}

		// Se obtiene el codigo de estado
		final HttpStatus status = jsonPost.getStatusCode();

		// Si no ha ido bien, se muestra error
		if (status == HttpStatus.OK) {
			// Se procesa la respuesta
			final String body = jsonPost.getBody();
			
			// se hace el parse
			final ObjectMapper mapper = new ObjectMapper().enable(SerializationConfig.Feature.INDENT_OUTPUT);
			final JsonNode loginResult = mapper.readValue(body, JsonNode.class);

			// Se obtienen el token y la url para las futuras peticiones
			connectionOut.setAccessToken(loginResult.get(Impl001FieldsEnum.ACCESS_TOKEN.getField()).asText());
			connectionOut.setInstanceUrl(loginResult.get(Impl001FieldsEnum.INSTANCE_URL.getField()).asText());

			LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"AccessToken",connectionOut.getAccessToken());
			LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"InstanceUrl", connectionOut.getInstanceUrl());
		} else {
			LOGGER.info(Impl001LogErrorEnum.BAD_RESPONSE_PATTERN.getPattern(),ERROR_HTTP,status.toString());
		}

		LOGGER.info(Impl001LogInfoEnum.REPO_FIN_METHOD.getPattern(),"createConnectionExternalAPI");
		return status;
	}
	
	/**
	 * Create Salesforce's job with external api connector
	 * Crear job en Salesforce con el conector
	 * @param apiConnectorExt
	 * @param createJobIn
	 * @return String JobId
	 * @throws URISyntaxException
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public String createJobExternalAPI(final APIConnector apiConnectorExt, final CreateJobInDTO createJobIn)
			throws URISyntaxException, ClientProtocolException, IOException {
		LOGGER.info(Impl001LogInfoEnum.REPO_INI_METHOD.getPattern(),"createJobExternalAPI");

		String idJob = null; // Id del job generado

		final Map<String, String> parametros = Impl001Utils.loadMapCreateJobParams(createJobIn.getObjeto(),
				createJobIn.getOperacion(), Impl001ValuesEnum.CONTENT_TYPE_CSV.getValue(),
				createJobIn.getExternalIdFieldName(), createJobIn.getColumnDelimiter(), createJobIn.getLineEnding());
		final String mensaje = Impl001Utils.generateMessageAuthorization(parametros);
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"Mensaje parametros",mensaje);

		final HttpHeaders headers = Impl001Utils.createHeaders(createJobIn.getAccessToken(), MediaType.APPLICATION_JSON,
				MediaType.APPLICATION_JSON);
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"HttpHeaders",headers);

		// Se establece un HttpEntity para aÃ±adir las cabeceras
		final HttpEntity<String> entity = new HttpEntity<String>(mensaje, headers);
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"HttpEntity",entity);

		// invocacion al api
		final ResponseEntity<String> jsonPost;
		try {
			jsonPost = apiConnectorExt.exchange(Impl001ValuesEnum.GET_SERVICE_INGEST.getValue(), HttpMethod.POST,
					entity, String.class, new Object[] { createJobIn.getInstanceUrl() });
		} catch (HttpClientErrorException e) {
			LOGGER.info(Impl001LogErrorEnum.EXCEPTION_MESSAGE_PATTERN.getPattern(),"HttpClientErrorException" ,e.getMessage());
			LOGGER.info(Impl001LogErrorEnum.EXCEPTION_TOSTRING_PATTERN.getPattern(),"HttpClientErrorException", e.toString());
			throw e;
		}
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"ResponseEntity",jsonPost);

		// Se obtiene el codigo de estado
		final HttpStatus status = jsonPost.getStatusCode();

		// Si no ha ido bien, se muestra error
		if (status == HttpStatus.OK) {
			// Se procesa la respuesta
			final String body = jsonPost.getBody();

			// se hace el parse
			final ObjectMapper mapper = new ObjectMapper().enable(SerializationConfig.Feature.INDENT_OUTPUT);
			final JsonNode createJobResult = mapper.readValue(body, JsonNode.class);

			// Se obtiene el id del Job generado
			idJob = createJobResult.get(Impl001FieldsEnum.ID.getField()).asText();
		} else {
			LOGGER.info(Impl001LogErrorEnum.BAD_RESPONSE_PATTERN.getPattern(),ERROR_HTTP,status.toString());
		}
		
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"Id Job",idJob);
		LOGGER.info(Impl001LogInfoEnum.REPO_FIN_METHOD.getPattern(),"createJobExternalAPI");
		return idJob;
	}

	/**
	 * It return OK (Code 201) if the send file to job was successful
	 * Devuelve OK (codigo 201) si el fichero se envío bien al job
	 * @param apiConnectorExt
	 * @param sendFileIn
	 * @return String Response  HttpStatus.CREATED
	 * @throws URISyntaxException
	 * @throws UnsupportedEncodingException
	 */
	public String sendFileExternalAPI(final APIConnector apiConnectorExt, final SendFileInDTO sendFileIn)
			throws URISyntaxException, UnsupportedEncodingException,IOException {
		LOGGER.info(Impl001LogInfoEnum.REPO_INI_METHOD.getPattern(),"sendFileExternalAPI");

		String response = null;

		final String url = sendFileIn.getIdJob() + Impl001ValuesEnum.PATH_BATCHES.getValue();

		final Map<String, String> paramsUrl = new HashMap<String, String>();
		paramsUrl.put(Impl001FieldsEnum.HOST_PARAMETER.getField(), sendFileIn.getInstanceUrl());
		paramsUrl.put(Impl001FieldsEnum.URL_PARAMETER.getField(), url);
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"Parametros URL",paramsUrl);

		// invocacion al api
		final ResponseEntity<String> jsonPut;
		try {

			jsonPut = apiConnectorExt.execute(Impl001ValuesEnum.GET_SERV_INGEST_2.getValue(), HttpMethod.PUT,
					Impl001Utils.requestCallback(sendFileIn.getContent(), sendFileIn.getAccessToken()),
					new ResponseExtractor<ResponseEntity<String>>() {
						public ResponseEntity<String> extractData(ClientHttpResponse response) throws IOException {
							Charset utf8charset = Charset.forName(Impl001ValuesEnum.ENCODING_UTF8.getValue());
							String mensaje = StreamUtils.copyToString(response.getBody(), utf8charset);
							LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"ResponseEntity",mensaje);
							return new ResponseEntity<String>(mensaje, response.getStatusCode());
						}
					}, paramsUrl);
		} catch (HttpClientErrorException e) {
			LOGGER.info(Impl001LogErrorEnum.EXCEPTION_MESSAGE_PATTERN.getPattern(),"HttpClientErrorException" ,e.getMessage());
			LOGGER.info(Impl001LogErrorEnum.EXCEPTION_TOSTRING_PATTERN.getPattern(),"HttpClientErrorException", e.toString());
			throw e;
		}

		if (jsonPut == null) {
			LOGGER.info("Send File External API: Response null");
			response = Impl001ResponseCodeEnum.KO.toString();
		} else {
			// Se obtiene el codigo de estado
			final HttpStatus status = jsonPut.getStatusCode();
			LOGGER.info(jsonPut.toString());
			LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"HttpStatus",status);

			// Si no ha ido bien, se muestra error
			if (status == HttpStatus.CREATED) {
				response = Impl001ResponseCodeEnum.OK.toString();
			} else {
				LOGGER.info(ERROR_HTTP, status.toString());

				response = Impl001ResponseCodeEnum.KO.toString();
			}
		}
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"Response",response);
		LOGGER.info(Impl001LogInfoEnum.REPO_FIN_METHOD.getPattern(),"sendFileExternalAPI");
		return response;
	}

	/**
	 * It close Salesforce's Job
	 * @param apiConnectorExt
	 * @param closeJobIn
	 * @return String
	 * @throws URISyntaxException
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public String closeJobExternalAPI(final APIConnector apiConnectorExt, final CloseJobInDTO closeJobIn)
			throws URISyntaxException, ClientProtocolException, IOException {
		LOGGER.info(Impl001LogInfoEnum.REPO_INI_METHOD.getPattern(),"closeJobExternalAPI");
		
		String response = null; // Respuesta de la llamada

		final Map<String, String> params = new HashMap<String, String>();
		params.put(Impl001FieldsEnum.STATE.getField(), Impl001ValuesEnum.ESTADO_JOB_UPLOAD_COMPLETE.getValue());
		final String mensaje = Impl001Utils.generateMessageAuthorization(params);
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"params httpEntity",mensaje);

		final HttpHeaders headers = Impl001Utils.createHeaders(closeJobIn.getAccessToken(), MediaType.APPLICATION_JSON,
				MediaType.APPLICATION_JSON);
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"HttpHeaders",headers);

		// Se establece un HttpEntity para aÃ±adir las cabeceras
		final HttpEntity<String> requestApi = new HttpEntity<String>(mensaje, headers);
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"HttpEntity",requestApi);

		final Map<String, String> paramsUrl = new HashMap<String, String>();
		paramsUrl.put(Impl001FieldsEnum.HOST_PARAMETER.getField(), closeJobIn.getInstanceUrl());
		paramsUrl.put(Impl001FieldsEnum.URL_PARAMETER.getField(), closeJobIn.getId() + "/");
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"Parametros URL",paramsUrl);

		// invocacion al api
		final ResponseEntity<String> jsonPatch;
		try {
			jsonPatch = apiConnectorExt.exchange(Impl001ValuesEnum.GET_SERV_INGEST_2.getValue(), HttpMethod.PATCH,
					requestApi, String.class, paramsUrl);
		} catch (HttpClientErrorException e) {
			LOGGER.info(Impl001LogErrorEnum.EXCEPTION_MESSAGE_PATTERN.getPattern(),"HttpClientErrorException" ,e.getMessage());
			LOGGER.info(Impl001LogErrorEnum.EXCEPTION_TOSTRING_PATTERN.getPattern(),"HttpClientErrorException", e.toString());
			throw e;
		}
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"ResponseEntity",jsonPatch);

		// Se obtiene el codigo de estado
		final HttpStatus status = jsonPatch.getStatusCode();
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"HttpStatus",jsonPatch.getStatusCode());

		// Si no ha ido bien, se muestra error
		if (status == HttpStatus.OK) {
			response = Impl001ResponseCodeEnum.OK.toString();
		} else {
			LOGGER.info(ERROR_HTTP, status.toString());
			response = Impl001ResponseCodeEnum.KO.toString();
		}
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"Response",response);
		LOGGER.info(Impl001LogInfoEnum.REPO_FIN_METHOD.getPattern(),"closeJobExternalAPI");
		return response;
	}
	
	/**
	 * It wait until salesforce's job was finished
	 * @param apiConnectorExt
	 * @param closeJobIn
	 * @return ShowResultsOutDTO
	 * @throws URISyntaxException
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public ShowResultsOutDTO awaitStatusExternalAPI(final APIConnector apiConnectorExt, final CloseJobInDTO closeJobIn)
			throws URISyntaxException, ClientProtocolException, IOException {
		LOGGER.info(Impl001LogInfoEnum.REPO_INI_METHOD.getPattern(), "awaitStatusExternalAPI");
		ShowResultsOutDTO showResultsOut = new ShowResultsOutDTO();

		final HttpHeaders headers = Impl001Utils.createHeaders(closeJobIn.getAccessToken(), MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON);
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(), "HttpHeaders", headers);

		// Se establece un HttpEntity para añadir las cabeceras
		final HttpEntity<String> requestApi = new HttpEntity<String>(headers);
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(), "HttpEntity", requestApi);

		final Map<String, String> paramsUrl = new HashMap<String, String>();
		paramsUrl.put(Impl001FieldsEnum.HOST_PARAMETER.getField(), closeJobIn.getInstanceUrl());
		paramsUrl.put(Impl001FieldsEnum.URL_PARAMETER.getField(), closeJobIn.getId() + "/");
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(), "Parametros URL", paramsUrl);

		String estadoJob = "";
		long recordsOk = 0, recordsFailed = 0;
		final ObjectMapper mapper = new ObjectMapper().enable(SerializationConfig.Feature.INDENT_OUTPUT);
		JsonNode createJobResult = null;
		while (estadoJob.equalsIgnoreCase("") || estadoJob.equalsIgnoreCase(Impl001ValuesEnum.ESTADO_JOB_OPEN.getValue())
				|| estadoJob.equalsIgnoreCase(Impl001ValuesEnum.ESTADO_JOB_UPLOAD_COMPLETE.getValue())
				|| estadoJob.equalsIgnoreCase(Impl001ValuesEnum.ESTADO_JOB_IN_PROGRESS.getValue())) {

			// invocacion al api
			final ResponseEntity<String> jsonGet;
			try {
				jsonGet = apiConnectorExt.exchange(Impl001ValuesEnum.GET_SERV_INGEST_2.getValue(), HttpMethod.GET, requestApi, String.class, paramsUrl);
			} catch (HttpClientErrorException e) {
				LOGGER.info(Impl001LogErrorEnum.EXCEPTION_MESSAGE_PATTERN.getPattern(),"HttpClientErrorException" ,e.getMessage());
				LOGGER.info(Impl001LogErrorEnum.EXCEPTION_TOSTRING_PATTERN.getPattern(),"HttpClientErrorException", e.toString());
				throw e;
			}
			LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(), "ResponseEntity", jsonGet);

			final HttpStatus status = jsonGet.getStatusCode();
			LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(), "HttpStatus", status);

			if (status == HttpStatus.OK) {
				// Se procesa la respuesta
				final String body = jsonGet.getBody();

				// se hace el parse
				createJobResult = mapper.readValue(body, JsonNode.class);
				LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(), "CreateJobResult", createJobResult);

				// get nodes for validate the constructing the correct files
				recordsOk = createJobResult.get(Impl001FieldsEnum.OK_RECORDS.getField()).asLong();
				recordsFailed = createJobResult.get(Impl001FieldsEnum.FAILED_RECORDS.getField()).asLong();

				// Se obtiene el estado del Job generado
				estadoJob = createJobResult.get(Impl001FieldsEnum.STATE.getField()).asText();

				// Se obtiene el codigo de estado
				LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(), "Estado Job", estadoJob);

				showResultsOut.setResponseCode(Impl001ResponseCodeEnum.OK.toString());
				showResultsOut.setStateJob(estadoJob);
			} else {
				LOGGER.info(ERROR_HTTP, status.toString());
				showResultsOut.setResponseCode(Impl001ResponseCodeEnum.KO.toString());
			}
		}

		// SET DATA TO showResultsOut
		Impl001Utils.showResumeJob(createJobResult, showResultsOut);

		// Successful Results
		if (recordsOk > 0)
			showResultExternalAPI(apiConnectorExt, closeJobIn, showResultsOut, Impl001ValuesEnum.PATH_SUCCESSFUL_RESULTS.getValue());
		// Failed Results
		if (recordsFailed > 0)
			showResultExternalAPI(apiConnectorExt, closeJobIn, showResultsOut, Impl001ValuesEnum.PATH_FAILED_RESULTS.getValue());

		// Unprocessed Results
		showResultExternalAPI(apiConnectorExt, closeJobIn, showResultsOut, Impl001ValuesEnum.PATH_UNPROCESSED_RESULTS.getValue());

		LOGGER.info(Impl001LogInfoEnum.REPO_FIN_METHOD.getPattern(), "awaitStatusExternalAPI");

		return showResultsOut;
	}

	/**
	 * It build a java object with the information about the Salesforce's job
	 * @param apiConnectorExt
	 * @param closeJobIn
	 * @param showResultsOut
	 * @param pathResults
	 * @return ShowResultsOutDTO
	 * @throws URISyntaxException
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	private void showResultExternalAPI(final APIConnector apiConnectorExt,
			final CloseJobInDTO closeJobIn, final ShowResultsOutDTO showResultsOut, final String pathResults)
			throws URISyntaxException, ClientProtocolException, IOException {
		LOGGER.info(Impl001LogInfoEnum.REPO_INI_METHOD.getPattern(),"showResultExternalAPI");

		final Map<String, String> paramsUrl = new HashMap<String, String>();
		paramsUrl.put(Impl001FieldsEnum.HOST_PARAMETER.getField(), closeJobIn.getInstanceUrl());
		paramsUrl.put(Impl001FieldsEnum.URL_PARAMETER.getField(), closeJobIn.getId() + pathResults);
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"Parametros URL",paramsUrl.toString());

		// invocacion al api
		final ResponseEntity<String> jsonGet;
		try {
			jsonGet = apiConnectorExt.execute(Impl001ValuesEnum.GET_SERV_INGEST_2.getValue(), HttpMethod.GET,
					Impl001Utils.requestCallback(null,closeJobIn.getAccessToken()), new ResponseExtractor<ResponseEntity<String>>(){
			    public ResponseEntity<String> extractData(    ClientHttpResponse response) throws IOException {
			    	Charset utf8charset = Charset.forName(Impl001ValuesEnum.ENCODING_UTF8.getValue());
					String mensaje = StreamUtils.copyToString(response.getBody(), utf8charset);
//					LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"ResponseEntity",mensaje);
					return new ResponseEntity<String>(mensaje,response.getStatusCode());
				    }
				}, paramsUrl);
		} catch (HttpClientErrorException e) {
			LOGGER.info(Impl001LogErrorEnum.EXCEPTION_MESSAGE_PATTERN.getPattern(),"HttpClientErrorException" ,e.getMessage());
			LOGGER.info(Impl001LogErrorEnum.EXCEPTION_TOSTRING_PATTERN.getPattern(),"HttpClientErrorException", e.toString());
			throw e;
		}
//		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"ResponseEntity",jsonGet);

		final HttpStatus status = jsonGet.getStatusCode();
		LOGGER.info(Impl001LogInfoEnum.REPO_MESSAGE.getPattern(),"HttpStatus",status);

		if (status == HttpStatus.OK) {
			// Se procesa la respuesta
			final String body = jsonGet.getBody();
//			LOGGER.info("-- Body1: {}", body);
			if (body == null || body.equalsIgnoreCase("")) {
				LOGGER.info("Response body null or is empty");
			} else {
				final InputStream inputStream = IOUtils.toInputStream(body,Impl001ValuesEnum.ENCODING_UTF8.getValue());
				final List<String> lines = IOUtils.readLines(inputStream, Impl001ValuesEnum.ENCODING_UTF8.getValue());
//				LOGGER.info("-- Lines: {}", lines.toString());

				if (pathResults.equalsIgnoreCase(Impl001ValuesEnum.PATH_SUCCESSFUL_RESULTS.getValue())) {
					showResultsOut.setLinesResult(lines);
				} else if (pathResults.equalsIgnoreCase(Impl001ValuesEnum.PATH_FAILED_RESULTS.getValue())) {
					showResultsOut.setLinesFailed(lines);
				} else {
					showResultsOut.setLinesUnprocessed(lines);
				}
				showResultsOut.setResponseCode(Impl001ResponseCodeEnum.OK.toString());
			}
		} else {
			LOGGER.info(ERROR_HTTP, status);
			showResultsOut.setResponseCode(Impl001ResponseCodeEnum.KO.toString());
		}
		LOGGER.info(Impl001LogInfoEnum.REPO_FIN_METHOD.getPattern(),"showResultExternalAPI");
	}
	
	
}
