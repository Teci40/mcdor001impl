package com.bbva.mcdo.lib.r001.impl;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.ResourceAccessException;

import com.bbva.apx.exception.io.network.TimeoutException;
import com.bbva.elara.utility.api.connector.APIConnector;
import com.bbva.mcdo.lib.r001.impl.repository.Impl001Repository;
import com.bbva.mcdo.lib.r001.impl.utils.Impl001LogErrorEnum;
import com.bbva.mcdo.lib.r001.impl.utils.Impl001LogInfoEnum;
import com.bbva.mcdo.lib.r001.impl.utils.Impl001ResponseCodeEnum;
import com.bbva.mcdo.lib.r001.impl.utils.Impl001Utils;
import com.bbva.mcdo.dto.salesforce.closeJob.CloseJobInDTO;
import com.bbva.mcdo.dto.salesforce.comun.ResponseCodeOutDTO;
import com.bbva.mcdo.dto.salesforce.connection.ConnectionInDTO;
import com.bbva.mcdo.dto.salesforce.connection.ConnectionOutDTO;
import com.bbva.mcdo.dto.salesforce.createJob.CreateJobInDTO;
import com.bbva.mcdo.dto.salesforce.createJob.CreateJobOutDTO;
import com.bbva.mcdo.dto.salesforce.sendFile.SendFileInDTO;
import com.bbva.mcdo.dto.salesforce.showResults.ShowResultsOutDTO;

public class MCDOR001Impl extends MCDOR001Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger(MCDOR001Impl.class);
	
	private Impl001Repository impl001Repository;

	private transient APIConnector extApiConnector;

	public void setImpl001Repository(Impl001Repository impl001Repository) {
		this.impl001Repository = impl001Repository;
	}

	private short attemptsExecute = 7;
	
	public void setExtApiConnector(APIConnector extApiConnector) {
		this.extApiConnector = extApiConnector;
	}

	@Override
	public ConnectionOutDTO executeCreateLoginConnection(final ConnectionInDTO connectionIn)
			throws IOException {
		LOGGER.info(Impl001LogInfoEnum.IMPL_INI_METHOD.getPattern(),"executeCreateLoginConnection");
		final ConnectionOutDTO connectionOut = new ConnectionOutDTO();	
		boolean successfulResult = false;
		if (validateConnectionIn(connectionIn)) {
			HttpStatus httpStatus = null;
			for(int i=1; (i<=attemptsExecute) && (!successfulResult);i++){
				LOGGER.info(Impl001LogInfoEnum.IMPL_ATTEMPT.getPattern(),i);
				try {
					httpStatus = impl001Repository.createConnectionExternalAPI(extApiConnector,
							connectionIn, connectionOut);
					successfulResult = true;
				} catch (SocketTimeoutException | ResourceAccessException | TimeoutException e) {
					LOGGER.error(Impl001LogErrorEnum.EXCEPTION_MESSAGE_PATTERN.getPattern(),"SocketTimeoutException",e.getMessage());
					
				}
			}
			if (httpStatus != HttpStatus.OK) {
				LOGGER.info(Impl001LogErrorEnum.BAD_RESPONSE_PATTERN.getPattern(),"ERROR_HTTP_RESPONSE_FAILED",httpStatus);
			}
		}
		LOGGER.info(Impl001LogInfoEnum.IMPL_FIN_METHOD.getPattern(),"executeCreateLoginConnection");
		return connectionOut;
	}
	
	private boolean validateConnectionIn(final ConnectionInDTO connectionIn) {
		boolean result = true;

		if (connectionIn == null) {
			LOGGER.info(Impl001LogErrorEnum.VALIDATION_OBJECT_NULL_PATTERN.getPattern(),"ConnectionInDTO");
			result = false;
		} else {
			if (connectionIn.getConsumerKey() == null || connectionIn.getConsumerKey().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"ConsumerKey");
				result = false;
			} else if (connectionIn.getConsumerSecret() == null || connectionIn.getConsumerSecret().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"ConsumerSecret");
				result = false;
			} else if (connectionIn.getUsername() == null || connectionIn.getUsername().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"Username");
				result = false;
			} else if (connectionIn.getPassword() == null || connectionIn.getPassword().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"Password");
				result = false;
			}
		}
		return result;
	}

	@Override
	public CreateJobOutDTO executeCreateJob(final CreateJobInDTO createJobIn) throws URISyntaxException, IOException {
		LOGGER.info(Impl001LogInfoEnum.IMPL_INI_METHOD.getPattern(),"executeCreateJob");
		final CreateJobOutDTO createJobOut = new CreateJobOutDTO();
		String idJob = null;
		boolean successfulResult = false;

		if (validateCreateJobIn(createJobIn)) {
			for (int i = 1; (i <= attemptsExecute) && (!successfulResult); i++) {
				LOGGER.info(Impl001LogInfoEnum.IMPL_ATTEMPT.getPattern(),i);
				try {
					idJob = impl001Repository.createJobExternalAPI(extApiConnector, createJobIn);
					successfulResult = true;
				} catch (SocketTimeoutException | ResourceAccessException | TimeoutException e) {
					LOGGER.error(Impl001LogErrorEnum.EXCEPTION_MESSAGE_PATTERN.getPattern(),"SocketTimeoutException",e.getMessage());
					
				}
			}
			if (idJob == null) {
				LOGGER.info(Impl001LogErrorEnum.BAD_RESPONSE_PATTERN.getPattern(),"ERROR_HTTP_RESPONSE_FAILED", "Id Job NULL");
			} else {
				createJobOut.setIdJob(idJob);
			}
		}
		LOGGER.info(Impl001LogInfoEnum.IMPL_FIN_METHOD.getPattern(),"executeCreateJob");
		return createJobOut;
	}

	private boolean validateCreateJobIn(final CreateJobInDTO createJobIn) {
		boolean result = true;
		if (createJobIn == null) {
			LOGGER.info(Impl001LogErrorEnum.VALIDATION_OBJECT_NULL_PATTERN.getPattern(),"CreateJobInDTO");
			result = false;
		} else {
			if (createJobIn.getAccessToken() == null || createJobIn.getAccessToken().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"AccessToken");
				result = false;
			} else if (createJobIn.getInstanceUrl() == null || createJobIn.getInstanceUrl().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"InstanceUrl");
				result = false;
			} else if (createJobIn.getObjeto() == null || createJobIn.getObjeto().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"Objeto");
				result = false;
			} else if (createJobIn.getOperacion() == null || createJobIn.getOperacion().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"Operacion");
				result = false;
			}
		}
		return result;
	}

	@Override
	public ResponseCodeOutDTO executeSendFile(final SendFileInDTO sendFileIn) throws IOException, URISyntaxException {
		LOGGER.info(Impl001LogInfoEnum.IMPL_INI_METHOD.getPattern(),"executeSendFile");
		final ResponseCodeOutDTO responseCodeOut = new ResponseCodeOutDTO();
		boolean successfulResult = false;
		if (validateSendFileIn(sendFileIn)) {
			for (int i = 1; (i <= attemptsExecute) && (!successfulResult); i++) {
				LOGGER.info(Impl001LogInfoEnum.IMPL_ATTEMPT.getPattern(),i);
				try {
					responseCodeOut.setResponseCode(impl001Repository.sendFileExternalAPI(extApiConnector, sendFileIn));
					successfulResult = true;
				} catch (SocketTimeoutException | ResourceAccessException | TimeoutException e) {
					LOGGER.error(Impl001LogErrorEnum.EXCEPTION_MESSAGE_PATTERN.getPattern(),"SocketTimeoutException",e.getMessage());
					
				}
			}
			if (responseCodeOut.getResponseCode() == null){
				LOGGER.info(Impl001LogErrorEnum.BAD_RESPONSE_PATTERN.getPattern(), "NULL");
				return responseCodeOut;
			}
			if (responseCodeOut.getResponseCode().equalsIgnoreCase(Impl001ResponseCodeEnum.KO.toString())) {
				LOGGER.info(Impl001LogErrorEnum.BAD_RESPONSE_PATTERN.getPattern(),"ERROR_HTTP_RESPONSE", responseCodeOut.getResponseCode());
			}
		}
		LOGGER.info(Impl001LogInfoEnum.IMPL_FIN_METHOD.getPattern(),"executeSendFile");
		return responseCodeOut;
	}

	private boolean validateSendFileIn(final SendFileInDTO sendFileIn) {
		boolean result = true;
		if (sendFileIn == null) { 
			LOGGER.info(Impl001LogErrorEnum.VALIDATION_OBJECT_NULL_PATTERN.getPattern(),"SendFileInDTO");
			result = false;
		} else {
			if (sendFileIn.getAccessToken() == null || sendFileIn.getAccessToken().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"AccessToken");
				result = false;
			} else if (sendFileIn.getInstanceUrl() == null || sendFileIn.getInstanceUrl().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"InstanceUrl");
				result = false;
			} else if (sendFileIn.getIdJob() == null || sendFileIn.getIdJob().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"IdJob");
				result = false;
			} else if (sendFileIn.getContent() == null || sendFileIn.getContent().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"Content");
				result = false;
			}
		}
		return result;
	}

	@Override
	public ResponseCodeOutDTO executeCloseJob(final CloseJobInDTO closeJobIn) throws URISyntaxException, IOException {
		LOGGER.info(Impl001LogInfoEnum.IMPL_INI_METHOD.getPattern(),"executeCloseJob");
		final ResponseCodeOutDTO responseCodeOut = new ResponseCodeOutDTO();
		boolean successfulResult = false;
		
		if (validateCloseJobIn(closeJobIn)) {
			for (int i = 1; (i <= attemptsExecute) && (!successfulResult); i++) {
				LOGGER.info(Impl001LogInfoEnum.IMPL_ATTEMPT.getPattern(),i);
				try {
					responseCodeOut.setResponseCode(impl001Repository.closeJobExternalAPI(extApiConnector, closeJobIn));
					successfulResult = true;
				} catch (SocketTimeoutException | ResourceAccessException | TimeoutException e) {
					LOGGER.error(Impl001LogErrorEnum.EXCEPTION_MESSAGE_PATTERN.getPattern(),"SocketTimeoutException");
					
				}
			}
			if (responseCodeOut.getResponseCode() == null){
				LOGGER.info(Impl001LogErrorEnum.BAD_RESPONSE_PATTERN.getPattern(),"ResponseCode", "NULL");
				return responseCodeOut;
			}
			if (responseCodeOut.getResponseCode().equalsIgnoreCase(Impl001ResponseCodeEnum.KO.toString())) {
				LOGGER.info(Impl001LogErrorEnum.BAD_RESPONSE_PATTERN.getPattern(),"ERROR_HTTP ", responseCodeOut.getResponseCode());
			}
		}
		LOGGER.info(Impl001LogInfoEnum.IMPL_FIN_METHOD.getPattern(),"executeCloseJob");
		return responseCodeOut;
	}

	@Override
	public ShowResultsOutDTO executeShowResults(final CloseJobInDTO closeJobIn) throws IOException, URISyntaxException {
		LOGGER.info(Impl001LogInfoEnum.IMPL_INI_METHOD.getPattern(),"executeShowResults");
		ShowResultsOutDTO showResultsOut = null;
		boolean successfulResult = false;

		if (validateCloseJobIn(closeJobIn)) {
			// await status and show result (two tries)
			for (int i = 1; (i <= attemptsExecute) && (!successfulResult); i++) {
				LOGGER.info(Impl001LogInfoEnum.IMPL_ATTEMPT.getPattern(),i);
				try {
					showResultsOut = impl001Repository.awaitStatusExternalAPI(extApiConnector, closeJobIn);
					successfulResult = true;
				} catch (SocketTimeoutException | ResourceAccessException | TimeoutException e) {
					LOGGER.error(Impl001LogErrorEnum.EXCEPTION_MESSAGE_PATTERN.getPattern(),"SocketTimeoutException",e.getMessage());
					
				}
			}
			if(showResultsOut ==null){
				LOGGER.info(Impl001LogErrorEnum.BAD_RESPONSE_PATTERN.getPattern(),"showResultsOut", "NULL");
				return showResultsOut;
			}
			if (showResultsOut.getResponseCode().equalsIgnoreCase(Impl001ResponseCodeEnum.KO.toString())) {
				LOGGER.info(Impl001LogErrorEnum.BAD_RESPONSE_PATTERN.getPattern(),"ERROR_HTTP_RESPONSE", showResultsOut.getResponseCode());
			}
		}
		LOGGER.info(Impl001LogInfoEnum.IMPL_FIN_METHOD.getPattern(),"executeShowResults");
		return showResultsOut;
	}

	private boolean validateCloseJobIn(final CloseJobInDTO closeJobIn) {
		boolean result = true;
		if (closeJobIn == null) {
			LOGGER.info(Impl001LogErrorEnum.VALIDATION_OBJECT_NULL_PATTERN.getPattern(),"CloseJobInDTO");
			result = false;
		} else {
			if (closeJobIn.getAccessToken() == null || closeJobIn.getAccessToken().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"AccessToken");
				result = false;
			} else if (closeJobIn.getInstanceUrl() == null || closeJobIn.getInstanceUrl().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"InstanceUrl");
				result = false;
			} else if (closeJobIn.getId() == null || closeJobIn.getId().isEmpty()) {
				LOGGER.info(Impl001LogErrorEnum.VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN.getPattern(),"IdJob");
				result = false;
			}
		}
		return result;
	}
	
	
}
