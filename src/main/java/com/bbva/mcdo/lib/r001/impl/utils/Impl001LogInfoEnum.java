package com.bbva.mcdo.lib.r001.impl.utils;

public enum Impl001LogInfoEnum {
	IMPL_INI_METHOD		("\t INI : {}"),
	IMPL_FIN_METHOD		("\t FIN : {}"),
	IMPL_ATTEMPT		(" -- \t ATTEMPT REQUEST NUMBER : {}"),
	REPO_INI_METHOD		(" -- \t INI : {}"),
	REPO_FIN_METHOD		(" -- \t FIN : {}"),
	REPO_MESSAGE		("-- -- -- -- \t {} : {}"),
	UTILS_INI_METHOD	("-- -- -- -- -- \t INI : {}"),
	UTILS_FIN_METHOD	("-- -- -- -- -- \t FIN : {}"),
	UTILS_MESSAGE		("-- -- -- -- -- -- -- -- \t {} : {}"),
	
	
	;
	
	private String pattern;

	public String getPattern() {
		return pattern;
	}

	Impl001LogInfoEnum(String pattern) {
		this.pattern = pattern;
	}

}
