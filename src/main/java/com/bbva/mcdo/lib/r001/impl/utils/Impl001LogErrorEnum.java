package com.bbva.mcdo.lib.r001.impl.utils;

public enum Impl001LogErrorEnum {
	VALIDATION_FIELD_NULL_OR_EMPTY_PATTERN	("##### ERROR MDCMR001 ###### {} FIELD NOT FOUND"),
	VALIDATION_OBJECT_NULL_PATTERN			("##### ERROR MDCMR001 ##### {} OBJECT IS NULL"),
	EXCEPTION_MESSAGE_PATTERN				("######### ERROR MDCMR001 {} ERROR MESSAGE: {} #########"),
	EXCEPTION_TOSTRING_PATTERN				("######### ERROR MDCMR001 {} ERROR TOSTRING: {} #########"),
	BAD_RESPONSE_PATTERN					("########### ERROR MDCMR001 {}: {} ###########"),
	
	;
	
	private String pattern;
	
	public String getPattern() {
		return pattern;
	}
	
	Impl001LogErrorEnum(String pattern){
		this.pattern = pattern;
	}

}
