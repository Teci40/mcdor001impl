package com.bbva.mcdo.lib.r001.impl.utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RequestCallback;


import com.bbva.mcdo.dto.salesforce.closeJob.CloseJobInDTO;
import com.bbva.mcdo.dto.salesforce.connection.ConnectionInDTO;
import com.bbva.mcdo.dto.salesforce.showResults.ShowResultsOutDTO;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.JsonNode;

public class Impl001Utils {
	private static final Logger LOGGER = LoggerFactory.getLogger(Impl001Utils.class);
	/**
	 * add data to Object with job resume of Salesforce
	 * @param createJobResult
	 * @param showResultsOut
	 */
	public static void showResumeJob(JsonNode createJobResult ,ShowResultsOutDTO showResultsOut){
		List<String> data = new ArrayList<String>();
		//Set header to file
		data.add(Impl001ValuesEnum.MESSAGE_HEADER.getValue());
		Iterator<String> fieldNames = createJobResult.getFieldNames();
		while (fieldNames.hasNext()) {
			String field = fieldNames.next();
			String value = createJobResult.get(field).asText();
			data.add(Impl001ValuesEnum.TAB.getValue()+field + ": " + value);
		}
		showResultsOut.setJobResume(data);
	}
	
	/**
	 * Convert Map to String in json format
	 * @param parametros
	 * @return String jsonFormat
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 */
	public static String generateMessageAuthorization(final Map<String, String> parametros)
			throws JsonGenerationException, JsonMappingException, IOException {
		String mensaje = "";
		mensaje = new ObjectMapper().writeValueAsString(parametros);

		return mensaje;
	}
	
	/**
	 * Establece las cabeceras de una llamada
	 * Set headers to request
	 * 
	 * @param attachment
	 * @param createJobIn
	 * @return HttpHeaders
	 */
	public static HttpHeaders createHeaders(final String accessToken, final MediaType contentTypeHeader,
			final MediaType accept) {
		LOGGER.info(Impl001LogInfoEnum.UTILS_INI_METHOD.getPattern(),"createHeaders");

		// Cabeceras iniciales
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(contentTypeHeader);
		headers.set(Impl001FieldsEnum.AUTHORIZATION.getField(), Impl001ValuesEnum.BEARER.getValue() + accessToken);

		List<MediaType> mediaTypes = new ArrayList<MediaType>();
		mediaTypes.add(accept);
		headers.setAccept(mediaTypes);

		// Codificacion de caracteres
		final List<Charset> charsets = new ArrayList<Charset>();
		charsets.add(Charset.forName(Impl001ValuesEnum.ENCODING_UTF8.getValue()));
		headers.setAcceptCharset(charsets);
		LOGGER.info(Impl001LogInfoEnum.UTILS_MESSAGE.getPattern(),"PARAM-AcceptCharset","UTF-8");
		
		LOGGER.info(Impl001LogInfoEnum.UTILS_MESSAGE.getPattern(),"headers",headers);
		LOGGER.info(Impl001LogInfoEnum.UTILS_FIN_METHOD.getPattern(),"createHeaders");
		return headers;
	}
	
	/**
	 * build Map with job parameters to create Salesforce job
	 * contruir un Map para crear el job en Salesforce
	 * @param object
	 * @param operation
	 * @param contentType
	 * @param externalID
	 * @param columnDelimiter
	 * @param lineEnding
	 * @return Map <String, String>
	 */
	public static Map<String, String> loadMapCreateJobParams(final String object, final String operation,
			final String contentType, final String externalID, final String columnDelimiter, final String lineEnding) {
		final Map<String, String> parametros = new HashMap<String, String>();
		parametros.put(Impl001FieldsEnum.OBJECT.getField(), object);
		parametros.put(Impl001FieldsEnum.CONTENT_TYPE.getField(), contentType);
		parametros.put(Impl001FieldsEnum.OPERATION.getField(), operation);
		if (externalID != null && !externalID.isEmpty()) {
			parametros.put(Impl001FieldsEnum.EXTERNAL_ID.getField(), externalID);
		}
		if (columnDelimiter != null && !columnDelimiter.isEmpty()) {
			parametros.put(Impl001FieldsEnum.COLUNM_DELIMITER.getField(), columnDelimiter);
		}
		if (lineEnding != null && !lineEnding.isEmpty()) {
			parametros.put(Impl001FieldsEnum.LINE_ENDING.getField(), lineEnding);
		}
		return parametros;
	}
	
	/**
	 * Build  RequestCallback of Spring with headers and body
	 * @param mensaje
	 * @param accessToken
	 * @return RequestCallback
	 * @see https://www.baeldung.com/rest-template
	 */
	public static RequestCallback requestCallback(final String mensaje, final String accessToken) {

		return new RequestCallback() {
			@Override
			public void doWithRequest(ClientHttpRequest clientHttpRequest) throws IOException {
				MediaType mediaType = new MediaType(Impl001ValuesEnum.CONTENT_TYPE_TEXT.getValue(),
						Impl001ValuesEnum.CONTENT_TYPE_CSV.getValue().toLowerCase(),
						Charset.forName(Impl001ValuesEnum.ENCODING_UTF8.getValue()));
				MediaType csv = new MediaType(Impl001ValuesEnum.CONTENT_TYPE_TEXT.getValue(),
						Impl001ValuesEnum.CONTENT_TYPE_CSV.getValue().toLowerCase());
				if (mensaje != null && !mensaje.isEmpty())
					StreamUtils.copy(mensaje, Charset.forName(Impl001ValuesEnum.ENCODING_UTF8.getValue()),
							clientHttpRequest.getBody());
				List<MediaType> mediaTypesAccept = new ArrayList<MediaType>();
				mediaTypesAccept.add(mediaType);
				final List<Charset> charsets = new ArrayList<Charset>();
				charsets.add(Charset.forName(Impl001ValuesEnum.ENCODING_UTF8.getValue()));
				clientHttpRequest.getHeaders().setAcceptCharset(charsets);
				clientHttpRequest.getHeaders().setAccept(mediaTypesAccept);
				clientHttpRequest.getHeaders().setContentType(csv);
				clientHttpRequest.getHeaders().add(Impl001FieldsEnum.AUTHORIZATION.getField(),
						Impl001ValuesEnum.BEARER.getValue() + accessToken);
				LOGGER.info(Impl001LogInfoEnum.UTILS_MESSAGE.getPattern(),"HttpHeaders Callback",clientHttpRequest.getHeaders());
			}
		};
	}
	
}
