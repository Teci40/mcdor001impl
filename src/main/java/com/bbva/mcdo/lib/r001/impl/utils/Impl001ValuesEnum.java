package com.bbva.mcdo.lib.r001.impl.utils;

public enum Impl001ValuesEnum {
	
	// enums para token
	BEARER("Bearer "),
	
	// paths
	PATH("/services/data/v40.0/jobs/ingest/"),
	PATH_QUERY("/services/data/v40.0/query/"),
	PATH_SUCCESSFUL_RESULTS("/successfulResults/"),
	PATH_FAILED_RESULTS("/failedResults/"),
	PATH_UNPROCESSED_RESULTS("/unprocessedrecords/"),
	PATH_BATCHES("/batches"),
	GET_URL_SALESFORCE("getUrlSalesforce"),
	GET_SERVICE_INGEST("getServiceIngest"),
	GET_SERV_INGEST_2("getServIngest2"),
	GET_SERVICE_QUERY("getServiceQuery"),
	GET_SERVICE_DYNAMIC_QUERY("getServiceDynamicQuery"),
	GET_SERVICE_QUERY_PAGGING("getPaggingQuery"),
	
	// encoding
		ENCODING_UTF8("UTF-8"),
		
	// Contents types header
	CONTENT_TYPE_HEADER_JSON_CHARSET("application/json; charset=UTF-8"),
	CONTENT_TYPE_HEADER_FORM_CHARSET("application/x-www-form-urlencoded;charset=utf-8"),
	
	// contents types
	CONTENT_TYPE_CSV("CSV"),
	CONTENT_TYPE_APPLICATION_JSON("application/json"),
	CONTENT_TYPE_TEXT("text"),
	CONTENT_TYPE_TEXT_CSV("text/csv"),
	CONTENT_TYPE_URLENCODED("application/x-www-form-urlencoded"),
	
	// Estados Job Salesforce
	ESTADO_JOB_OPEN("Open"),
	ESTADO_JOB_UPLOAD_COMPLETE("UploadComplete"),
	ESTADO_JOB_IN_PROGRESS("InProgress"),
	ESTADO_JOB_JOBCOMPLETE("JobComplete"),
	ESTADO_JOB_201("201"),
	ESTADO_JOB_FAILED("Failed"),
	ESTADO_JOB_ABORTED("Aborted"),
		
	//Query
	REPLACE_QUERY("\\|"),
	FIELD_ATTRIBUTES("attributes"),
	WHERE_SENTENCE(" WHERE "),
		
	//Parametros job Predefinidos
	ALL("all"),
	DEFAULT("default"),
	
	//Header of Status file
	MESSAGE_HEADER("INFORMATION STATUS JOB"),
	TAB("\t"),
		
		;
	
	private String value;
	
	public String getValue() {
		return value;
	}
	
	Impl001ValuesEnum(String value){
		this.value = value;
	}
	
	
	}
