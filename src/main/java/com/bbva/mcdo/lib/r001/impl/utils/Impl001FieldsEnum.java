package com.bbva.mcdo.lib.r001.impl.utils;

public enum Impl001FieldsEnum {
	
	CLIENT_ID("client_id"),
	CLIENT_SECRET("client_secret"),
	GRAND_TYPE("grant_type"),
	USERNAME("username"),
	USER_SF_OBJECT("User"),
	PASSWORD("password"),
	ACCESS_TOKEN("access_token"),
	INSTANCE_URL("instance_url"),
	OBJECT("object"),
	FIELDS_PARAMETER("fields"),
	CONTENT_TYPE("contentType"),
	OPERATION("operation"),
	EXTERNAL_ID("externalIdFieldName"),
	AUTHORIZATION("Authorization"),
	CONTENT_TYPE_HEADER("Content-Type"),
	ACCEPT("Accept"),
	ID("id"), //NODO RESPONSE JOB
	ID_Field("Id"), //NODO JOB RESPONSE QUERY
	STATE("state"),
	OK_RECORDS("numberRecordsProcessed"),
	FAILED_RECORDS("numberRecordsFailed"),
	ERROR_MESSAGE("errorMessage"),
	PROXY_HOST("api.connector.proxy.host"),
	PROXY_PORT("api.connector.proxy.port"),
	PROXY_USER("api.connector.proxy.username"),
	PROXY_PASS("api.connector.proxy.password"),
	HOST_PARAMETER("host"),
	URL_PARAMETER("url"),
	COLUNM_DELIMITER("columnDelimiter"),
	LINE_ENDING("lineEnding"),
	QUERY_FIELD("query"),
	PATH_IN("sf.inPath"),
	PATH_OUT("sf.outPath"),
	
	//campos para where en query
	WHERE_FIELD("where"),
	ID_USER_CREATED("createdbyId"),
	ID_USER_MODIFIED("LastModifiedById"),
	CREATED_DATE("createdDate"),
	MODIFIED_DATE("LastModifiedDate"),
	DAY_ONLY("day_only")
	;
	
	private String field;
	
	public String getField() {
		return field;
	}

	Impl001FieldsEnum(String field){
		this.field = field;
	}


}
